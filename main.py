import re
import sys
import fileinput
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

import click

from copy import deepcopy
from typing import List, Optional
from time import sleep


def extract_numbers(string: str) -> Optional[List[float]]:
    """
    Extracts all floats from dirty strings, e.g. 'sth: 1.23, sth else: 1e-5'
    """
    findings = re.findall(r"(-?\d+\.?\d*(e(\+|\-)?\d+)?)", string)
    if findings:
        return [float(match[0]) for match in findings]
    return None


def process_line(
    string: str, sequences: List[List[Optional[float]]]
) -> List[List[Optional[float]]]:
    numbers = extract_numbers(string)

    if not numbers:
        return sequences

    sequences = deepcopy(sequences)

    while len(sequences) < len(numbers):
        sequences.append([])

    length_longest_list = 0
    for seq in sequences:
        if len(seq) > length_longest_list:
            length_longest_list = len(seq)

    for seq in sequences:
        while len(seq) < length_longest_list:
            seq.append(None)

    for idx, num in enumerate(numbers):
        sequences[idx].append(num)

    return sequences


def mypause(interval):
    backend = plt.rcParams["backend"]
    if backend in matplotlib.rcsetup.interactive_bk:
        figManager = matplotlib._pylab_helpers.Gcf.get_active()
        if figManager is not None:
            canvas = figManager.canvas
            if canvas.figure.stale:
                canvas.draw()
            canvas.start_event_loop(interval)
            return


def update_plot(sequences, axes):
    color_cycle = plt.rcParams["axes.prop_cycle"].by_key()["color"]

    while len(axes.lines) < len(sequences):
        empty_line = Line2D([], [])
        empty_line.set_color(color_cycle[len(axes.lines) % 10])
        axes.add_line(empty_line)

    for idx, lines in enumerate(axes.lines):
        current_seq = sequences[idx]

        lines.set_xdata(range(len(current_seq)))
        lines.set_ydata(current_seq)

    axes.relim()
    axes.autoscale_view()

    mypause(1e-9)


@click.command()
@click.option(
    "-v", "--verbose", is_flag=True, default=False, help="Print incoming data to stdout"
)
@click.argument("file", type=click.File("r"), default="-")
def main(verbose, file):
    if file is sys.stdin and sys.stdin.isatty():
        print("Error: Called directly. Please pipe data into me.. Or pass a file")
        exit(1)

    plt.ion()
    plt.style.use("ggplot")

    plt.figure()
    axes = plt.gca()

    sequences = []

    for line in file:
        if verbose:
            print(line)

        sequences = process_line(line, sequences)
        update_plot(sequences, axes)

    plt.ioff()
    plt.show()


if __name__ == "__main__":
    main(None, None)
