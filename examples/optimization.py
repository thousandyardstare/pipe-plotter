import random

random.seed(1234)


def main():
    for val in reversed(range(100)):
        print("Optimization error: {}".format((val / 100) ** 2 + random.random() * 0.1))


if __name__ == "__main__":
    main()
