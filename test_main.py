from hypothesis.strategies import floats, text, characters
from hypothesis import given, example

import unittest
import main


class TestEzPlot(unittest.TestCase):

    @given(text(characters(blacklist_categories=["Nd"], blacklist_characters="-")))
    def test_process_line(self, string):
        str_result = main.process_line(string, [])
        self.assertEqual(str_result, [])

        single_num = main.process_line("lel2.34", [])
        self.assertEqual(single_num, [[2.34]])

        single_appended = main.process_line("4.44 5.55", single_num)
        self.assertEqual(single_appended, [[2.34, 4.44], [None, 5.55]])

        double_num = main.process_line("lel 1.34  3.45 adsf", [])
        self.assertEqual(double_num, [[1.34], [3.45]])

        double_appended = main.process_line("1.11 2.22", double_num)
        self.assertEqual(double_appended, [[1.34, 1.11], [3.45, 2.22]])

    @given(
        floats(allow_infinity=False, allow_nan=False),
        text(characters(blacklist_categories=["Nd"], blacklist_characters="-")),
        text(characters(blacklist_categories=["Nd"], blacklist_characters="-")),
    )
    def test_main(self, float_a, str_a, str_b):
        some_number_str = str(float_a)

        self.assertIsNotNone(main.extract_numbers(some_number_str))
        self.assertEqual(main.extract_numbers(some_number_str), [float_a])
        self.assertIsNone(main.extract_numbers(str_a))

        mixed_str = str(float_a) + str_a
        self.assertEqual(main.extract_numbers(mixed_str), [float_a])

        mixed_str2 = str_a + str(float_a) + str_b
        self.assertEqual(main.extract_numbers(mixed_str2), [float_a])
